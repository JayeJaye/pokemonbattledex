package com.example.jay.battledex.DatabaseTables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class Ability {

    @PrimaryKey
    private Integer abilityId;

    private String abilityName;
}
