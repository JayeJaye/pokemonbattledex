package com.example.jay.battledex.DatabaseTables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity (tableName = "Types")
public class Type {

    @PrimaryKey
    private String typeId;

    private String typeName;

    private String typeIcon;

}
