package com.example.jay.battledex.DatabaseTables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity (tableName = "All possible moves")
public class AllMoves {

    @PrimaryKey
    private Integer moveId;

    private String moveName;

    private String defaultPP;

    private String moveCategory;

    private String moveType;

    private String movePower;
}
