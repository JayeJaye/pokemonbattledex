package com.example.jay.battledex.DatabaseTables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity (tableName = "Single Pokemon", foreignKeys = @ForeignKey(entity = Team.class,
        parentColumns = "teamId", childColumns = "teamId", onDelete = CASCADE, onUpdate = CASCADE))
public class PokemonInstance {

    @PrimaryKey
    private Integer instanceId;

    private String name;

    private String nickname;

    private String natDexNumber;

    private String moveId;

    private String teamId;
}
