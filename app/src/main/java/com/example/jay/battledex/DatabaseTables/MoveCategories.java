package com.example.jay.battledex.DatabaseTables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class MoveCategories {

    @PrimaryKey
    private Integer categoryId;

    private String categoryName;
}
