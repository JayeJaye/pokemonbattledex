package com.example.jay.battledex.DatabaseTables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity (tableName = "Learnable Moves", foreignKeys = { @ForeignKey(entity = NatDexEntry.class,
parentColumns = "natDexEntryId", childColumns = "possiblePokemon", onDelete = CASCADE), @ForeignKey(
        entity = AllMoves.class, parentColumns = "moveId", childColumns = "moveId")})
public class LearnableMoves {

    @PrimaryKey
    private Integer movePossibilityId;

    private String moveId;

    private String possiblePokemon;
}
