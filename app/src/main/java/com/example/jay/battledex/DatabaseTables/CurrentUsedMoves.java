package com.example.jay.battledex.DatabaseTables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity (tableName = "Moves in use", foreignKeys = { @ForeignKey(entity = AllMoves.class,
parentColumns = "moveId", childColumns = "moveId"), @ForeignKey(entity = NatDexEntry.class,
parentColumns = "natDexEntryId", childColumns = "pokemonId")})
public class CurrentUsedMoves {

    @PrimaryKey
    private Integer moveUseId;

    private String moveId;

    private String pokemonId;
}
