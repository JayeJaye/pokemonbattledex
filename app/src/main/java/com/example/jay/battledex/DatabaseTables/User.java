package com.example.jay.battledex.DatabaseTables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity (tableName = "Users")
public class User {

    @PrimaryKey
    private String userId;

    private String userName;

    private String userEmail;

    private String userPassword;

    private String userTeams;
}
