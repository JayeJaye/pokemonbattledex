package com.example.jay.battledex.DatabaseTables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity (tableName = "National Dex Entries")
public class NatDexEntry {

    @PrimaryKey
    private Integer natDexEntryId;

    private Integer natDexNumber;

    private String pokemonName;

    private String pokemonType;

    private String pokemonImage;

    private String availableMoves;

    private String effectivenessProfile;

    private String version;

}
