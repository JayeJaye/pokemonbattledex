package com.example.jay.battledex.DatabaseTables;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity (tableName = "Effectiveness", foreignKeys = { @ForeignKey(entity = Type.class,
        parentColumns = "typeId", childColumns = "typeId", onDelete = CASCADE, onUpdate = CASCADE),
@ForeignKey(entity = NatDexEntry.class, parentColumns = "natDexEntryId", childColumns = "pokemonId",
onUpdate = CASCADE, onDelete = CASCADE)})
public class EffectivenessProfile {

    @PrimaryKey
    private Integer effectivenessRelationshipId;

    private String typeId;

    private String effectiveness;

    private String pokemonId;
}
