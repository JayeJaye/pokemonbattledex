package com.example.jay.battledex.DatabaseTables;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "Teams", foreignKeys = @ForeignKey(entity = User.class, parentColumns = "userId",
childColumns = "ownerId", onUpdate = CASCADE, onDelete = CASCADE))

public class Team {

    @PrimaryKey
    private String teamId;

    private String ownerId;

    private String teamName;

    private String pokemonInstanceId;

}
